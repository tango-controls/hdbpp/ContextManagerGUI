# Changelog

#### 1.4 - 15/02/2024:
	Compile in Java 17 (JRE >= 17 required)
	Update pom.xml after the move to sonatype
	Remove dependency to HDBViewer and add dependency to jcalendar
	Update Tag Name format
	Add CI

#### ContextManagerGUI-1.3 - 20/12/2019:
    Add a parameter to be launched from another process

#### ContextManagerGUI-1.2 - 19/12/2019:
    Edit calendar by double click on JD synoptic

#### ContextManagerGUI-1.1 - 26/11/2019:
    Replace JLChart by a JDraw synoptic to display contexts

#### ContextManagerGUI-1.0 - 07/11/2019:
    Initial Revision
