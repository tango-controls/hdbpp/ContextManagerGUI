# Project ContextManagerGUI

Maven Java project

This application is a GUI for ContextManager device server.
 - It is able to show context for all event subscribers.
 - It is also able to set the context periods.



## Cloning

```
git clone git@gitlab.com:tango-controls/hdbpp/ContextManagerGUI.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Jive.jar
* JTango.jar
* ATKCore.jar
* ATKWidget.jar
* JCalendar.jar
  

#### Toolchain Dependencies 

* javac 8 or higher
* maven
  

### Build


Instructions on building the project.

```
cd ContextManagerGUI
mvn package
```

