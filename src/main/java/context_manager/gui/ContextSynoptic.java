//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2018,2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package context_manager.gui;

import context_manager.commons.Context;
import context_manager.commons.Utils;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is able to generate code for a JDraw file displaying a context list
 *
 * @author verdier
 */

public class ContextSynoptic extends SynopticFileViewer {
    private List<Context> contextList = new ArrayList<>();
    private static final int Y_OFFSET = 25;
    private static final String TEMPLATE_PERIOD =
            "  JDRectangle {\n" +                   //  Context rectangle
            "    summit:10,$Y1,130,$HEIGHT\n" +
            "    origin:59,59\n" +
            "    background:$BACKGROUND\n" +
            "    fillStyle:1\n" +
            "    name:\"Period\"\n" +
            "  }\n" +
            "  JDLabel {\n" +                       //  Context type object
            "    summit:12,$Y1,127,$Y4\n" +
            "    origin:68,19\n" +
            "    lineWidth:0\n" +
            "    name:\"ContextName\"\n" +
            "    font:\"Dialog\",1,12\n"+
            "    text:\"$NAME\"\n" +
            "  }\n"+
            "  JDLabel {\n" +                       //  DATE object
            "    summit:130,$Y2,240,$Y3\n" +
            "    origin:64,35\n" +
            "    lineWidth:0\n" +
            "    name:\"ContextDate\"\n" +
            "    font:\"Dialog\",1,12\n"+
            "    text:\"$DATE\"\n" +
            "  }\n";
    private static final String NOW_PERIOD =
            "  JDRectangle {\n" +                   //  Rectangle for actual context
            "    summit:10,$Y1,130,$HEIGHT\n" +
            "    origin:59,59\n" +
            "    foreground:255,51,255\n" +
            "    lineWidth:5\n" +
            "    name:\"NowPeriod\"\n" +
            "  }\n";
    private static final String HEADER_FILE =
            "JDFile v11 {\n" +
            "  Global {\n" +
            "  }\n";
    private static final int[] HEIGHTS = { 50, 20, 50 };
    private static final int LABEL_HEIGHT = 24;
    private static final double ZOOM_FACTOR = 0.8;
    private static final String TMP_FILE = "/tmp/context.jdw";
    //===============================================================
    //===============================================================
    public ContextSynoptic(List<Context> contextList) throws DevFailed {
        this.contextList.addAll(contextList);
        generateCode();
        try {
            setJdrawFileName(TMP_FILE);
            setAutoZoom(true);
            setAutoZoomFactor(ZOOM_FACTOR);
            if (!new File(TMP_FILE).delete())
                System.err.println("Cannot delete " + TMP_FILE);
        }
        catch (FileNotFoundException e) {
            Except.throw_exception(e.toString(), e.toString());
        }
    }
    //===============================================================
    //===============================================================
    private Context getNowContext() {
        long now = System.currentTimeMillis();
        Context previousContext = null;
        for (Context context : contextList) {
            if (context.getStartTime()<now) {
                previousContext = context;
            }
        }
        return previousContext;
    }
    //===============================================================
    //===============================================================
    private String addNow() {
        StringBuilder sb = new StringBuilder();
        Context nowContext = getNowContext();
        if (nowContext!=null) {
            int y = Y_OFFSET;
            for (Context context : contextList) {
                if (context==nowContext) {
                    String code = NOW_PERIOD;
                    code = Utils.strReplace(code, "$Y1", Integer.toString(y));
                    y += HEIGHTS[context.getType()];
                    code = Utils.strReplace(code, "$HEIGHT", Integer.toString(y));
                    sb.append(code);
                    y += 2;  //  For nex one
                }
                else {
                    y += HEIGHTS[context.getType()] + 2;  //  For nex one
                }
            }
        }
        return sb.toString();
    }
    //===============================================================
    //===============================================================
    private void generateCode() throws DevFailed {
        StringBuilder sb = new StringBuilder(HEADER_FILE);
        int y = Y_OFFSET;
        for (Context context : contextList) {
            String code = TEMPLATE_PERIOD;
            String strBg = Integer.toString(context.getBackground().getRed()) + ',' +
                    context.getBackground().getGreen() + ',' +
                    context.getBackground().getBlue();
            code = Utils.strReplace(code, "$Y1", Integer.toString(y));
            code = Utils.strReplace(code, "$Y2", Integer.toString(y-LABEL_HEIGHT/2));
            code = Utils.strReplace(code, "$Y3", Integer.toString(y+LABEL_HEIGHT/2));
            code = Utils.strReplace(code, "$Y4", Integer.toString(y+LABEL_HEIGHT));
            code = Utils.strReplace(code, "$BACKGROUND", strBg);
            code = Utils.strReplace(code, "$NAME", context.getName());
            code = Utils.strReplace(code, "$DATE", context.getStartString());

            y += HEIGHTS[context.getType()];
            code = Utils.strReplace(code, "$HEIGHT", Integer.toString(y));

            y += 2;  //  For nex one
            sb.append(code);
        }
        sb.append(addNow());
        sb.append( "};");
        Utils.writeFile(TMP_FILE, sb.toString());
    }
    //===============================================================
    //===============================================================
    public int getSynopticWidth() {
        return 220;
    }
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {
        try {
            List<Context> contextList = new ArrayList<>();
            contextList.add(new Context("Shutdown", "01/11/19  08:00"));
            contextList.add(new Context("Restart", "21/11/19  08:00"));
            contextList.add(new Context("Run", "28/11/19  16:00"));
            contextList.add(new Context("Shutdown", "18/12/19  08:00"));
            contextList.add(new Context("Restart", "13/01/20  08:00"));
            contextList.add(new Context("Run", "15/01/20  16:00"));
            new ContextSynoptic(contextList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //===============================================================
    //===============================================================

}
