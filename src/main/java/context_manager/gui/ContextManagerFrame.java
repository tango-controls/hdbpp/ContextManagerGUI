//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for main swing class.
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,......,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package context_manager.gui;

import context_manager.commons.*;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

//=======================================================
/**
 *	JFrame Class to display info
 *
 * @author  Pascal Verdier
 */
//=======================================================
public class ContextManagerFrame extends JFrame {
    private JFrame parent;
	private ErrorHistory errorHistory = new ErrorHistory();
	private int centerPanelHeight;
	private ContextSynoptic synopticViewer;

	private static final String ACS_MANAGER = System.getenv("ACS_MANAGER");
	//=======================================================
    /**
	 *	Creates new form ContextManagerGUIFrame
	 */
	//=======================================================
    public ContextManagerFrame(JFrame parent) throws DevFailed {
        this.parent = parent;
        try {
            SplashUtils.getInstance().startSplash();
            SplashUtils.getInstance().setSplashProgress(5, "Building GUI");
            SplashUtils.getInstance().startAutoUpdate();
            initComponents();

            //  Add event subscriber context viewers
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = gbc.gridy = 0;
            gbc.insets = new Insets(10, 20, 10, 20);
            gbc.anchor = GridBagConstraints.NORTH;
            String[] acsSubscribers = getAcsSubscriberDevices();

            //  Add a scalar table viewer for ACS IDs BMs
            JScrollPane scrollPane = buildScalarTableViewer(acsSubscribers);
            panel.add(scrollPane, gbc);
            gbc.gridx++;

            scrollPane = buildScalarTableViewer(getBeamLineSubscriberDevices("id", acsSubscribers).toArray(new String[0]));
            panel.add(scrollPane, gbc);
            gbc.gridx++;
            scrollPane = buildScalarTableViewer(getBeamLineSubscriberDevices("d", acsSubscribers).toArray(new String[0]));
            panel.add(scrollPane, gbc);

            getContentPane().add(panel, BorderLayout.CENTER);
            pack();
            centerPanelHeight = panel.getHeight();

            //  Generate and display a jdraw file to show contexts
            scrollPane = buildSynoptic();
            getContentPane().add(scrollPane, BorderLayout.EAST);

            //  Finalize frame
            setTitle(Utils.getInstance().getApplicationName());
            pack();
            ATKGraphicsUtils.centerFrameOnScreen(this);
            SplashUtils.getInstance().stopSplash();
        }
        catch (DevFailed e) {
            SplashUtils.getInstance().stopSplash();
            throw e;
        }
	}
	//=======================================================
	//=======================================================
    private JScrollPane buildSynoptic() throws DevFailed {
        synopticViewer = new ContextSynoptic(Context.getContextList());
        synopticViewer.setBackground(getBackground());
        synopticViewer.setToolTipText("Double click to edit contexts");
        synopticViewer.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                if (event.getClickCount()==2)
                    showContextTable();
            }
        });
        JScrollPane scrollPane = new JScrollPane(synopticViewer);
        scrollPane.setPreferredSize(new Dimension(synopticViewer.getSynopticWidth(), centerPanelHeight));
        return scrollPane;
    }
	//=======================================================
	//=======================================================
    private JScrollPane buildScalarTableViewer(String[] subscriberDevices) {
        List<String> rowTitles = new ArrayList<>();
        List<String> attributeList = new ArrayList<>();
        for (String subscriberDevice : subscriberDevices) {
            rowTitles.add(subscriberDevice.substring(subscriberDevice.lastIndexOf('/')+1));
            attributeList.add(subscriberDevice + "/Context");
        }
        ScalarTableViewer tableViewer = new ScalarTableViewer(
                attributeList, rowTitles.toArray(new String[0]), "Temp.", true, errorHistory);
        tableViewer.setValueColumnWidth(new int[]{60});
        JScrollPane scrollPane = new JScrollPane(tableViewer);
        scrollPane.setPreferredSize(tableViewer.getTableDimension());
        return scrollPane;
    }
	//=======================================================
	//=======================================================
    private String[] getAcsSubscriberDevices() throws DevFailed {
        // Get ACS at first using manager
        if (ACS_MANAGER==null || ACS_MANAGER.isEmpty())
            Except.throw_exception("PropertyNotSet",
                    "Environment variable ACS_MANAGER is not set !");
        return new DeviceProxy(ACS_MANAGER).read_attribute("ArchiverList").extractStringArray();
    }
	//=======================================================
	//=======================================================
    private List<String> getBeamLineSubscriberDevices(String type, String[] acsSubscribers) throws DevFailed {
        String[] deviceNames = ApiUtil.get_db_obj().get_device_list("sys/hdb-man/"+type+"*");
        List<String> list = new ArrayList<>();
        for (String deviceName : deviceNames) {
            if (!isInList(deviceName, acsSubscribers))
                list.add(deviceName);
        }
        return list;
    }
	//=======================================================
	//=======================================================
    private boolean isInList(String str, String[] list) {
        for (String s : list) {
            if (s.equalsIgnoreCase(str))
                return true;
        }
        return false;
    }
	//=======================================================
	//=======================================================
    private void showContextTable() {
        try {
            if (new ContextTableDialog(this).showDialog()==JOptionPane.OK_OPTION) {
                //  Update chart with modifications
                getContentPane().remove(synopticViewer.getParent().getParent());
                JScrollPane scrollPane = buildSynoptic();
                getContentPane().add(scrollPane, BorderLayout.EAST);
                pack();
            }
        }
        catch (DevFailed e) {
            ErrorPane.showErrorMessage(this, null, e);
        }
    }
	//=======================================================
	//=======================================================

	//=======================================================
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
	//=======================================================
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JMenuBar menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem setContextsItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem releaseItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem1 = new javax.swing.JMenuItem();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });
        getContentPane().setLayout(new java.awt.BorderLayout());

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        setContextsItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        setContextsItem.setMnemonic('S');
        setContextsItem.setText("Set context dates");
        setContextsItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setContextsItemActionPerformed(evt);
            }
        });
        fileMenu.add(setContextsItem);

        exitItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitItem.setMnemonic('E');
        exitItem.setText("Exit");
        exitItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitItem);

        menuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("help");

        releaseItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        releaseItem.setMnemonic('R');
        releaseItem.setText("Release Notes");
        releaseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                releaseItemActionPerformed(evt);
            }
        });
        helpMenu.add(releaseItem);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setMnemonic('A');
        jMenuItem1.setText("About");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItem1);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void setContextsItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setContextsItemActionPerformed
        showContextTable();
    }//GEN-LAST:event_setContextsItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitItemActionPerformed
        doClose();
    }//GEN-LAST:event_exitItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        doClose();
    }//GEN-LAST:event_exitForm
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String  message = Utils.getInstance().getApplicationName() + "\n" +
                "This application is a GUI for ContextManager device server\n" +
                "It is able to:\n" +
                " - show context for all event subscribers\n" +
                " - set the context periods\n" +
                "\nPascal Verdier - Accelerator Control Unit";
        JOptionPane.showMessageDialog(this, message, "Help Window", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void releaseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_releaseItemActionPerformed
        new PopupHtml(this).show(ReleaseNotes.htmlString, 450, 450);
    }//GEN-LAST:event_releaseItemActionPerformed
	//=======================================================
	//=======================================================
    private void doClose() {
        if (parent==null)
            System.exit(0);
        else {
            setVisible(false);
            dispose();
        }
    }
	//=======================================================
	//=======================================================



	//=======================================================
    /**
     * @param args the command line arguments
     */
	//=======================================================
    public static void main(String[] args) {
		try {
            UIManager.put("ToolTip.foreground", new ColorUIResource(Color.black));
            UIManager.put("ToolTip.background", new ColorUIResource(Utils.toolTipBackground));
      		new ContextManagerFrame(null).setVisible(true);
		}
		catch(DevFailed e) {
            ErrorPane.showErrorMessage(new Frame(), null, e);
			System.exit(0);
		}
    }
	//=======================================================
	//=======================================================





	//=======================================================
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
	//=======================================================

}
