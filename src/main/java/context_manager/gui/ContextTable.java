//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// Pascal Verdier: pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2018,2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package context_manager.gui;

import context_manager.commons.Context;
import context_manager.commons.Utils;
import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

/**
 * This class is able to display data on a table
 *
 * @author verdier
 */

public class ContextTable extends JTable {
    private JDialog parent;
    private DataTableModel tableModel;
    private List<Context> contextList;
    private Context nowContext;
    private int tableWidth = 0;
    private int selectedRow = -1;
    private TablePopupMenu popupMenu = new TablePopupMenu();

    private static final int[] COLUMN_WIDTH = {80, 100 };
    private static final String[] COLUMN_HEADERS = {
            "Context", "Start"
    };
    private static final Color firstColumnColor = new Color(0xdd, 0xdd, 0xdd);
    //===============================================================
    //===============================================================
    public ContextTable(JDialog parent, List<Context> contextList) {
        this.parent = parent;
        this.contextList = contextList;
        nowContext = getNowContext();
        // Create the table
         tableModel = new DataTableModel();
        setModel(tableModel);
        setRowSelectionAllowed(true);
        setDefaultRenderer(String.class, new LabelCellRenderer());
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                tableActionPerformed(event);
            }
        });
        //  Column header management
        getTableHeader().setFont(new Font("Dialog", Font.BOLD, 14));

        //  Set column width
        final Enumeration columnEnum = getColumnModel().getColumns();
        int i = 0;
        TableColumn tableColumn;
        while (columnEnum.hasMoreElements()) {
            tableWidth += COLUMN_WIDTH[i];
            tableColumn = (TableColumn) columnEnum.nextElement();
            tableColumn.setPreferredWidth(COLUMN_WIDTH[i++]);
        }
    }
    //===============================================================
    //===============================================================
    private Context getNowContext() {
        long now = System.currentTimeMillis();
        Context previousContext = null;
        for (Context context : contextList) {
            if (context.getStartTime()<now) {
                previousContext = context;
            }
        }
        return previousContext;
    }
    //===============================================================
    //===============================================================
    private void tableActionPerformed(MouseEvent event) {
        selectedRow = rowAtPoint(new Point(event.getX(), event.getY()));
        Context context = contextList.get(selectedRow);
        if (event.getClickCount() == 2 && event.getButton()==1) {
            InputContextDialog inputContextDialog = new InputContextDialog(parent, context);
            Context newContext = inputContextDialog.showDialog();
            if (newContext!=null) {
                contextList.sort(new ContextComparator());
                tableModel.fireTableDataChanged();
            }
        }
        else if (event.getButton()==3) {
            popupMenu.showMenu(event, context);
        }
        repaint();
    }
    //===============================================================
    //===============================================================
    public int getTableWidth() {
        return tableWidth;
    }
    //===============================================================
    //===============================================================
    private void addNewContext() {
        //  Get last context time
        long time;
        if (contextList.isEmpty())
            time = System.currentTimeMillis();
        else
            time = contextList.get(contextList.size()-1).getStartTime();
        InputContextDialog contextDialog = new InputContextDialog(parent, time);
        Context context = contextDialog.showDialog();
        if (context!=null) {
            contextList.add(context);
            contextList.sort(new ContextComparator());
            tableModel.fireTableDataChanged();
        }
    }
    //===============================================================
    //===============================================================
    private void removeContext(Context context) {
        contextList.remove(context);
        tableModel.fireTableDataChanged();
    }
    //===============================================================
    //===============================================================
    public List<Context> getContextList() {
        return contextList;
    }
    //===============================================================
    //===============================================================


    //==============================================================
    /**
     * The Table model
     */
    //==============================================================
    public class DataTableModel extends AbstractTableModel {
        //==========================================================
        @Override
        public int getColumnCount() {
            return COLUMN_HEADERS.length;
        }
        //==========================================================
        @Override
        public int getRowCount() {
            return contextList.size();
        }
        //==========================================================
        @Override
        public String getColumnName(int columnIndex) {
            if (columnIndex >= getColumnCount())
                return COLUMN_HEADERS[getColumnCount() - 1];
            else
                return COLUMN_HEADERS[columnIndex];
        }
        //==========================================================
        @Override
        public Object getValueAt(int row, int column) {
            //  Done by renderer
            return "";  //rowList.get(row)[column];
        }
        //==========================================================
        @Override
        public void setValueAt(Object value, int row, int column) {
            System.out.println(value + " (" + row + ", " + column + ")");
            //contextList.get(row)[column] = (String) value;
        }
        //==========================================================
        /**
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         *
         * @param column the specified co;umn number
         * @return the cell class at first row for specified column.
         */
        //==========================================================
        @Override
        public Class getColumnClass(int column) {
            if (isVisible()) {
                return getValueAt(0, column).getClass();
            } else
                return null;
        }
        //==========================================================
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
        //==========================================================
    }
    //==============================================================
    //==============================================================


    //==============================================================
    /**
     * Renderer to set cell color
     */
    //==============================================================
    public class LabelCellRenderer extends JLabel implements TableCellRenderer {
        //==========================================================
        public LabelCellRenderer() {
            //setFont(new Font("Dialog", Font.BOLD, 11));
            setOpaque(true); //MUST do this for background to show up.
        }
        //==========================================================
        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value,
                boolean isSelected, boolean hasFocus,
                int row, int column) {
            Context context = contextList.get(row);
            setIcon(null);
            setToolTipText(null);

            if (column == 0) {
                setText(" " + context.getName());
                try {
                    setBackground(context.getBackground());
                } catch (IndexOutOfBoundsException e) {
                    //  Unknown context type
                    setBackground(firstColumnColor);
                }
            }
            else {
                setText(" " + context.getStartString());
                setToolTipText("Double click to edit " + context.getName());
                if (row == selectedRow)
                    setBackground(selectionBackground);
                else
                if (context==nowContext)
                    setBackground(new Color(0xff, 0xcc, 0xcc));
                else
                    setBackground(Color.white);
            }
            return this;
        }
    }
    //==============================================================
    //==============================================================



    //===========================================================
    //===========================================================
    private static final int ADD_CONTEXT = 0;
    private static final int REMOVE_CONTEXT = 1;
    private static final int OFFSET = 2;    //  Label + separator

    private static String[] menuLabels = {
            "Add Context",
            "Remove Context",
    };

    private class TablePopupMenu extends JPopupMenu {
        private Context selectedContext = null;
        //=======================================================
        //=======================================================
        private TablePopupMenu() {
            JLabel title = new JLabel("");
            title.setFont(new Font("Dialog", Font.BOLD, 14));
            add(title);
            add(new JPopupMenu.Separator());

            for (String menuLabel : menuLabels) {
                JMenuItem item = new JMenuItem(menuLabel);
                item.addActionListener(this::menuActionPerformed);
                add(item);
            }
        }
        //======================================================
        public void showMenu(MouseEvent event, Context context) {
            selectedContext = context;
            ((JLabel)getComponent(0)).setText(context.toString());
            show((Component) event.getSource(), event.getX(), event.getY());
        }
        //======================================================
        private void menuActionPerformed(ActionEvent event) {
            //  Check component source
            Object obj = event.getSource();
            int commandIndex = 0;
            for (int i=0 ; i<menuLabels.length ; i++)
                if (getComponent(OFFSET + i) == obj)
                    commandIndex = i;

            switch (commandIndex) {
                case ADD_CONTEXT:
                    addNewContext();
                    break;
                case REMOVE_CONTEXT:
                    removeContext(selectedContext);
                    break;
            }
        }
    }
    //===============================================================
    //===============================================================


    //======================================================
    /**
     * Comparators class to sort Strings
     */
    //======================================================
    public static class ContextComparator implements Comparator<Context> {
        //==================================================
        public int compare(Context context1, Context context2) {
            if (context1==null)
                return 1;
            else if (context2==null)
                return -1;
            else
                return Long.compare(context1.getStartTime(), context2.getStartTime());
        }
        //==================================================
    }
    //======================================================
    //======================================================
}
