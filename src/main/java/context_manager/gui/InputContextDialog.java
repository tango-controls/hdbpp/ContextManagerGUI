//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  Basic Dialog Class to display info
//
// $Author: pascal_verdier $
//
// Copyright (C) :      2004,2005,......,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package context_manager.gui;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;
import context_manager.commons.Context;
import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import java.awt.*;
import java.util.*;

//===============================================================
/**
 *	JDialog Class to display info
 *
 *	@author  Pascal Verdier
 */
//===============================================================

public class InputContextDialog extends JDialog {
	private JDialog parent;
	private Context context;
	private JComboBox<String> comboBox;
	private JDateChooser dateChooser;
	//===============================================================
	/**
	 *	Creates new form InputTimeDialog
	 */
	//===============================================================
	public InputContextDialog(JDialog parent, long time) {
		this(parent, null, time);
	}
	//===============================================================
	/**
	 *	Creates new form InputTimeDialog
	 */
	//===============================================================
	public InputContextDialog(JDialog parent, Context context) {
		this(parent, context, 0);
	}
	//===============================================================
	/**
	 *	Creates new form InputTimeDialog
	 */
	//===============================================================
	private InputContextDialog(JDialog parent, Context context, long time) {
		super(parent, true);
		this.parent = parent;
		this.context = context;
		initComponents();

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = gbc.gridy = 0;
		gbc.insets = new Insets(20, 50, 20, 5);
		//gbc.fill = GridBagConstraints.HORIZONTAL;
		//	Add a JComboBox or a JLabel for context name
		addContextComboBox(gbc, (context==null? 0 :context.getType()));

		//	Add a input time panel
		addInputTime(gbc, (context==null? time : context.getStartTime()));

		titleLabel.setText("Context period definition");
		pack();
 		ATKGraphicsUtils.centerDialog(this);
	}
	//===============================================================
	//===============================================================
	private void addInputTime(GridBagConstraints gbc, long time) {
		gbc.gridy++;
		gbc.insets = new Insets(0, 50, 20, 5);
		JLabel label = new JLabel("Start date");
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		centerPanel.add(label, gbc);

		gbc.gridx++;
		gbc.insets = new Insets(0, 5, 20, 50);
		dateChooser = new JDateChooser(new JCalendar(),
				new Date(time), "dd/MM/yy  HH:00", new JSpinnerDateEditor());
		dateChooser.setFont(new Font("Dialog", Font.BOLD, 14));
		centerPanel.add(dateChooser, gbc);
	}
	//===============================================================
	//===============================================================
	private void addContextComboBox(GridBagConstraints gbc, int type) {
		comboBox = new JComboBox<>();
		for (String name : Context.contextNames)
			comboBox.addItem(name);
		comboBox.setSelectedIndex(type);
		centerPanel.add(comboBox, gbc);
	}
	//===============================================================
	//===============================================================
	public static String formatDate(long ms) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(ms);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = 0;//calendar.get(Calendar.MINUTE);
		//int seconds = calendar.get(Calendar.SECOND);
		//int millis = calendar.get(Calendar.MILLISECOND);
		return String.format("%02d/%02d/%02d  %02d:%02d",
				day, month, (year-2000), hour, minutes);
	}
	//===============================================================
	//===============================================================


	//===============================================================
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
	//===============================================================
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel topPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        centerPanel = new javax.swing.JPanel();
        javax.swing.JPanel bottomPanel = new javax.swing.JPanel();
        javax.swing.JButton okBtn = new javax.swing.JButton();
        javax.swing.JButton cancelBtn = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        titleLabel.setFont(new java.awt.Font("Dialog", 1, 18));
        titleLabel.setText("Dialog Title");
        topPanel.add(titleLabel);

        getContentPane().add(topPanel, java.awt.BorderLayout.NORTH);

        centerPanel.setLayout(new java.awt.GridBagLayout());
        getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

        okBtn.setText("OK");
        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okBtnActionPerformed(evt);
            }
        });
        bottomPanel.add(okBtn);

        cancelBtn.setText("Cancel");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });
        bottomPanel.add(cancelBtn);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	//===============================================================
	//===============================================================
    @SuppressWarnings("UnusedParameters")
	private void okBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okBtnActionPerformed
		try {
			if (context == null) {
				context = new Context((String) comboBox.getSelectedItem(),
						formatDate(dateChooser.getDate().getTime()));
			} else {
				context.setName((String) comboBox.getSelectedItem());
				context.setStart(formatDate(dateChooser.getDate().getTime()));
			}
			doClose();
		}
		catch (DevFailed e) {
			ErrorPane.showErrorMessage(this, null, e);
		}
	}//GEN-LAST:event_okBtnActionPerformed
	//===============================================================
	//===============================================================
	@SuppressWarnings("UnusedParameters")
    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
		doClose();
	}//GEN-LAST:event_cancelBtnActionPerformed
	//===============================================================
	//===============================================================
    @SuppressWarnings("UnusedParameters")
	private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
		doClose();
	}//GEN-LAST:event_closeDialog
	//===============================================================
	//===============================================================
	private void doClose() {
		if (parent==null)
			System.exit(0);
		else {
			setVisible(false);
			dispose();
		}
	}
	//===============================================================
	//===============================================================
	public Context showDialog() {
		setVisible(true);
		return context;
	}
	//===============================================================
	//===============================================================



	//===============================================================
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel centerPanel;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
	//===============================================================

}
