//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package context_manager.commons;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


/**
 * This class is a set of static utility methods
 *
 * @author verdier
 */

public class Utils {
    private static Utils instance = new Utils();
    public static final Color toolTipBackground = new Color(0xffffd0);
    //===============================================================
    //===============================================================
    public static Utils getInstance() {
        return instance;
    }
    //===============================================================
    //===============================================================
    public String getApplicationName() {
        String applicationName = getClass().getPackage().getImplementationTitle();
        if (applicationName == null)
            applicationName = "ContextManagerGUI";

        String release = getClass().getPackage().getImplementationVersion();
        if (release!=null)
            return applicationName + "-" + release;
        else
           return applicationName + " - not released";
    }
    //======================================================================
    //======================================================================


    //===============================================================
    //===============================================================
    public static String strReplace(String text, String old_str, String new_str) {
        if (text == null) return "";
        for (int pos = 0; (pos = text.indexOf(old_str, pos)) >= 0; pos += new_str.length())
            text = text.substring(0, pos) + new_str +
                    text.substring(pos + old_str.length());
        return text;
    }
    //===============================================================
    //===============================================================
    public static String replaceChar(String str, char src, char target) {
        int idx;
        while ((idx=str.indexOf(src))>0)
            str = str.substring(0, idx) + target + str.substring(++idx);
        return str;
    }
    //===============================================================
    /**
     * Format with time before date, and no millis
     * @param ms date to format
     * @return the formatted date
     */
    //===============================================================
    public static String formatDate(long ms, boolean withMillis) {
        //  Format the date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY  HH:mm:ss");
        String date = simpleDateFormat.format(new Date(ms));

        //  Compute and add milliseconds
        double doubleTime = (double)ms/1000;
        ms = (long)((doubleTime - (long)doubleTime)*1000);
        String millis = String.format("%03d", ms);

        if (withMillis)
            date += '.' + millis;
        return date;
    }
    //===============================================================
    /**
     * Format with date before time for file name
     * @return the formatted date
     */
    //===============================================================
    public static String formatFileDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd_HH-mm");
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }
    //===============================================================
    /**
     * Open a file and return text read as a list of lines.
     *
     * @param filename file to be read.
     * @return the file content read as a list of lines.
     * @throws DevFailed in case of failure during read file.
     */
    //===============================================================
    public static List<String> readFileLines(String filename) throws DevFailed {
        String code = readFile(filename);
        List<String> lines = new ArrayList<>();
        StringTokenizer stk = new StringTokenizer(code, "\n");
        while (stk.hasMoreTokens())
            lines.add(stk.nextToken().trim());
        return lines;
    }
    //===============================================================
    /**
     * Open a file and return text read.
     *
     * @param filename file to be read.
     * @return the file content read.
     * @throws DevFailed in case of failure during read file.
     */
    //===============================================================
    public static String readFile(String filename) throws DevFailed {
        String str = "";
        try {
            FileInputStream inputStream = new FileInputStream(filename);
            int nb = inputStream.available();
            byte[] buffer = new byte[nb];
            nb = inputStream.read(buffer);
            inputStream.close();
            if (nb > 0)
                str = new String(buffer);
        } catch (Exception e) {
            Except.throw_exception("", e.toString());
        }
        return str;
    }
    //===============================================================
    //===============================================================
    public static void writeFile(String fileName, String code) throws DevFailed {
        try {
            FileOutputStream fid = new FileOutputStream(fileName);
            fid.write(code.getBytes());
            fid.close();
        } catch (Exception e) {
            Except.throw_exception("", e.toString());
        }
    }
    //===============================================================
    //===============================================================
    public static void writeFile(String fileName, List<String> list) throws DevFailed {
        StringBuilder sb = new StringBuilder();
        for (String item : list)
            sb.append(item).append('\n');
        writeFile(fileName, sb.toString());
    }
    //===============================================================
    /**
     *  Build a simple dialog with component at center position
     * @param parent dialog parent
     * @param modal  true if dialog must be modal
     * @param title  dialog title
     * @param component component to be added at center
     * @return the dialog
     */
    //===============================================================
    public static JDialog buildDialog(JFrame parent, boolean modal, String title, Component component) {
        final JDialog dialog = new JDialog(parent, modal);
        JPanel dialogPanel = new JPanel(new BorderLayout());
        dialogPanel.add(component, BorderLayout.CENTER);
        JButton button = new JButton("Dismiss");
        button.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                dialog.setVisible(false);
                dialog.dispose();
            }
        });
        JPanel panel = new JPanel();
        panel.add(button);
        dialogPanel.add(panel, BorderLayout.SOUTH);
        if (title!=null) {
            JLabel label = new JLabel(title);
            label.setFont(new Font("dialog", Font.BOLD, 18));
            panel = new JPanel();
            panel.add(label);
            dialogPanel.add(panel, BorderLayout.NORTH);
        }
        dialog.setContentPane(dialogPanel);
        dialog.pack();

        return dialog;
    }
    //===============================================================
    //===============================================================
    public static boolean osIsUnix() {
        return !osIsWindows();
    }
    //===============================================================
    //===============================================================
    public static boolean osIsWindows() {
        String os = System.getProperty("os.name");
        return os.toLowerCase().startsWith("windows");
    }
    //===============================================================
    //===============================================================
    /**
     * Execute a shell command and throw exception if command failed.
     *
     * @param cmd shell command to be executed.
     * @throws DevFailed in case of execution failed
     */
    //===============================================================
    public static void executeShellCmdAndReturn(String cmd) throws DevFailed {
        try {
            Process process = Runtime.getRuntime().exec(cmd);

            // get command output stream and
            // put a buffered reader input stream on it.
            InputStream inputStream = process.getInputStream();
            new BufferedReader(new InputStreamReader(inputStream));

            // do not read output lines from command
            // Do not check its exit value
        }
        catch (IOException e) {
            Except.throw_exception("", e.toString());
        }
    }
    //===============================================================
    /**
     *	Execute a shell command and throw exception if command failed.
     *
     *	@param cmd	shell command to be executed.
     */
    //===============================================================
    public static String executeShellCmd(String cmd) throws DevFailed {
        try {
            Process process = Runtime.getRuntime().exec(cmd);

            // get command's output stream and
            // put a buffered reader input stream on it.
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();

            // read output lines from command
            String output;
            while ((output = reader.readLine()) != null) {
                //System.out.println(str);
                sb.append(output).append("\n");
            }

            // wait for end of command
            process.waitFor();

            // check its exit value
            int retVal;
            if ((retVal = process.exitValue()) != 0) {
                //	An error occurs try to read it
                InputStream errorStream = process.getErrorStream();
                reader = new BufferedReader(new InputStreamReader(errorStream));
                while ((output = reader.readLine()) != null) {
                    System.out.println(output);
                    sb.append(output).append("\n");
                }
                Except.throw_exception("", 
                        "the shell command\n" + cmd + "\nreturns : " + retVal + " !\n\n" + sb);
            }
            //System.out.println(sb);
            return sb.toString();
        }
        catch (Exception e) {
            if (e instanceof DevFailed)
                throw (DevFailed) e;
            else
                Except.throw_exception("", e.toString());
            return null;
        }
    }
     //===============================================================
    private static jive3.MainPanel jive = null;
    //===============================================================
    public static void startJive(String name, boolean forDevice) {
        //  Start jive and go to the device node
        if (jive==null) {
            jive = new jive3.MainPanel(false, false);
        }
        jive.setVisible(true);
        if (forDevice)
            jive.goToDeviceNode(name);
        else
            jive.goToServerNode(name);
    }
    //======================================================================
    //======================================================================
    public static void testDevice(Component component, String deviceName) throws DevFailed {
        JDialog dialog;
        Component frame = getMainFrame(component);
        if (frame instanceof JFrame)
            dialog = new JDialog((JFrame) frame, false);
        else
        if (frame instanceof JDialog)
            dialog = new JDialog((JDialog) frame, false);
        else
            dialog = new JDialog(new JFrame(), false);

        dialog.setTitle(deviceName + " Device Panel");
        dialog.setContentPane(new jive.ExecDev(deviceName));
        dialog.pack();
        dialog.setVisible(true);
        ATKGraphicsUtils.centerDialog(dialog);
    }
    //===================================================================
    //===================================================================
    private static Component getMainFrame(Component component) {
        if (component instanceof JFrame || component instanceof JDialog)
            return component;

        // search JFrame or JDialog
        Component parent = component.getParent();
        while (parent!=null) {
            if (parent instanceof JFrame || parent instanceof JDialog)
                return parent;
            parent = parent.getParent();
        }
        return component;
    }
    //===================================================================
    //===================================================================
    //===================================================================
    //===================================================================



    //  Multi lines tooltip methods
    private static final String tooltipHeader =
            "<html><BODY TEXT=\"#000000\" BGCOLOR=\"#FFFFD0\">";

    //===============================================================
    //===============================================================
    @SuppressWarnings("UnusedDeclaration")
    public static String buildToolTip(String text) {
        return buildToolTip(null, text);
    }

    //===============================================================
    //===============================================================
    public static String buildToolTip(String title, String text) {

        StringBuilder sb = new StringBuilder(tooltipHeader);
        if (title != null && title.length() > 0) {
            sb.append("<b><Center>")
                    .append("&nbsp;&nbsp;")
                    .append(strReplace(title, "\n", "&nbsp;&nbsp;<br>&nbsp;&nbsp;\n"))
                    .append("&nbsp;&nbsp;")
                    .append("</center></b><HR WIDTH=\"100%\">");
        }
        if (text != null && text.length() > 0) {
            text = Utils.strReplace(text, "\n", "&nbsp;&nbsp;<br>\n&nbsp;&nbsp;");
            sb.append("&nbsp;&nbsp;")
                    .append(Utils.strReplace(text, "\' \'", "\'&nbsp;&nbsp;\'")).append("&nbsp;&nbsp;");
        }
        return sb.toString();
    }
    //===============================================================
    //===============================================================
}
