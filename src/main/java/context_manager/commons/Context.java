//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2018,2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package context_manager.commons;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class is able to
 *
 * @author verdier
 */

public class Context {
    private String name;
    private String start;
    private long startTime;

    public static final int SHUTDOWN = 0;
    public static final int RESTART = 1;
    public static final int RUN = 2;
    public static final Color[] ContextColors = {
            new Color(0xdd, 0xdd, 0xaa),    //  Shutdown
            new Color(0xaa, 0xdd, 0xdd),    //  Restart
            new Color(0xdd, 0xaa, 0xdd),    //  Run
    };
    public static final String[] contextNames = {
            "Shutdown", "Restart", "Run"
    };
    //===============================================================
    //===============================================================
    public Context(String name, String start) throws DevFailed {
        this.name = name;
        this.start = start;
        parseStartTime(start);
    }
    //===============================================================
    //===============================================================
    public Context(String line) throws DevFailed {
        int index = line.indexOf(":");
        if (index<0)
            Except.throw_exception("BadParam", line + "\ncannot be parsed");
        name = line.substring(0, index).trim();
        start = line.substring(++index).trim();
        parseStartTime(start);
    }
    //===============================================================
    //===============================================================
    private void parseStartTime(String timeStr) throws DevFailed {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy  hh:mm");
            Date date = formatter.parse(timeStr);
            startTime = date.getTime();
        }
        catch (ParseException e) {
            Except.throw_exception("BadParam", e.toString());
        }
    }
    //===============================================================
    //===============================================================
    public String getName() {
        return name;
    }
    //===============================================================
    //===============================================================
    public void setName(String name) {
        this.name = name;
    }
    //===============================================================
    //===============================================================
    public long getStartTime() {
        return startTime;
    }
    //===============================================================
    //===============================================================
    public String getShortStartString() {
        return start.substring(0, start.indexOf(" "));
    }
    //===============================================================
    //===============================================================
    public String getStartString() {
        return start;
    }
    //===============================================================
    //===============================================================
    public void setStart(String start) throws DevFailed {
        parseStartTime(start);
        this.start = start;
    }
    //===============================================================
    //===============================================================
    public Color getBackground() {
        return ContextColors[getType()];
    }
    //===============================================================
    //===============================================================
    public int getType() {
        switch (name.toLowerCase()) {
            case "shutdown": return SHUTDOWN;
            case "restart": return RESTART;
            default: return RUN;
        }
    }
    //===============================================================
    //===============================================================
    @Override
    public String toString() {
        return name + ":\t" + start;
    }
    //===============================================================
    //===============================================================



    //===============================================================
    //===============================================================
    private static DeviceProxy contextProxy = null;
    //===============================================================
    //===============================================================
    public static DeviceProxy getContextProxy() throws DevFailed {
        if (contextProxy==null) {
            String contextDeviceName = System.getenv("CONTEXT_DEVICE");
            if (contextDeviceName==null || contextDeviceName.isEmpty())
                Except.throw_exception("PropertyNotSet",
                        "Environment variable CONTEXT_DEVICE is not set");
            contextProxy = new DeviceProxy(contextDeviceName);
        }
        return contextProxy;
    }
    //===============================================================
    //===============================================================
    public static List<Context> getContextList() throws DevFailed {
        DbDatum datum = getContextProxy().get_property("ContextDates");
        if (datum.is_empty())
            return null;
        String[] lines = datum.extractStringArray();

        List<Context> contextList = new ArrayList<>();
        for (String line : lines) {
            contextList.add(new Context(line));
        }
        return contextList;
    }
    //===============================================================
    //===============================================================
}
